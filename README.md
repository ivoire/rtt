RTT
===

This program allows to compute the number of RTT for each year. In fact, the number of RTT depends on each year.

Using RTT
=========

Installing
----------

Currently, you can only install from sources.

```shell
git clone https://gitlab.com/ivoire/rtt.git
cd rtt
python3 -m rtt --help
```

Usage
-----

To compute the current year RTT count, just run:

```shell
python3 -m rtt
```

This will output the number of RTT for a full time employee.


Configuration
-------------

In order to customize the default, values, you can add a new section to the init file rtt/config.ini.

```ini
[bigcorp]
weekdays = 0, 1, 2, 3, 4
days_per_year = 218
days_off_per_year = 25
year_start = 10/01

[part_time]
weekdays = 0, 1, 2, 3, 4
days_per_year = 173
days_off_per_year = 20
year_start = 10/01
```

The first profile is a full time employee when the fiscal year start on October
while the second one is a part-time employee, not working on wednesdays.

```shell
python3 -m rtt --profile bigcorp
python3 -m rtt --profile part_time
```
