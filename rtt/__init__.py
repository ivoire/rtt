import argparse
import configparser
from dataclasses import dataclass
import datetime
from pathlib import Path
from typing import List, Tuple

from rtt.public_holidays import is_public_holiday
from rtt.version import __version__


#####################
# Config and parser #
#####################
@dataclass
class Config:
    weekdays: List[int]
    days_per_year: int
    days_off_per_year: int
    year_start: Tuple[int, int]

    def __post_init__(self):
        self.weekdays = [int(v) for v in self.weekdays.split(",")]
        self.days_per_year = int(self.days_per_year)
        self.days_off_per_year = int(self.days_off_per_year)
        self.year_start = (
            int(self.year_start.split("/")[0]),
            int(self.year_start.split("/")[1]),
        )


def load_config(filepath: Path, profile: str) -> Config:
    try:
        config = configparser.ConfigParser()
        config.read(filepath)
    except configparser.Error as exc:
        raise NotImplementedError(exc)
    if not config.has_section(profile):
        raise NotImplementedError(f"Missing section '{profile}'")

    return Config(
        weekdays=config[profile].get("weekdays"),
        days_per_year=config[profile].get("days_per_year"),
        days_off_per_year=config[profile].get("days_off_per_year"),
        year_start=config[profile].get("year_start"),
    )


def setup_parser():
    parser = argparse.ArgumentParser(prog="rtt", description="RTT")

    parser.add_argument(
        "--version", action="version", version=f"%(prog)s, {__version__}"
    )

    parser.add_argument(
        "--config", type=Path, default=(Path(__file__).parent / "config.ini")
    )
    parser.add_argument("--profile", type=str, default="default")
    parser.add_argument("--year", type=int, default=None)
    return parser


###########
# Helpers #
###########
def is_working_day(config, date):
    if not date.weekday() in config.weekdays:
        return False
    return not is_public_holiday(date)


##############
# Entrypoint #
##############
def main():
    options = setup_parser().parse_args()
    config = load_config(options.config, options.profile)

    if options.year is not None:
        start = datetime.date(
            year=options.year, month=config.year_start[0], day=config.year_start[1]
        )
    else:
        now = datetime.datetime.now()
        if (now.month, now.day) > config.year_start:
            start = datetime.date(
                year=now.year, month=config.year_start[0], day=config.year_start[1]
            )
        else:
            start = datetime.date(
                year=now.year - 1, month=config.year_start[0], day=config.year_start[1]
            )

    days = [
        d
        for d in (start + datetime.timedelta(n) for n in range(365))
        if is_working_day(config, d)
    ]
    print(f"travaillés: {len(days)}")
    print(f"congés: {len(days) - config.days_per_year}")
    print(f"RTT: {len(days) - config.days_per_year - config.days_off_per_year}")
    return 0
